#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <map>
using std::map;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	// NOTE : map automatically sorts the inputs !!
	// TODO : -c and -cu tests fail!! **maybe changing cout to printf
	// 			: done.. test passed!

	//since the inputs are sorted for testing, we will use maps
	map<string,size_t> A;
	string x;
	// read inputs by lines
	while (getline(cin,x)){
		A[x]++; //assigns an interger value to the string input and increment by 1
	}
	//duplicate and uniques are opposites, so conditions fail
	if (dupsonly == 1 && uniqonly ==1) {
		return 0;
	}
	for (map<string,size_t>::iterator i = A.begin(); i!= A.end(); i++) {
		// command: ./uniq
		if (showcount == 0 && dupsonly == 0 && uniqonly == 0)
			cout << i->first << '\n';
		else if (showcount == 1){
			//command: ./unic -c -d
			if (dupsonly == 1){
				if (i->second > 1){
					printf("%7lu ",i->second);
					cout << i->first << '\n';
				}
			}
			//command: ./uniq -c -u
			else if (uniqonly == 1){
				if (i->second == 1){
					cout << "      " << i->second << ' ' << i->first << '\n';
				}
			}
			//command: ./uniq -c
			else {
				printf("%7lu ",i->second);
				cout << i->first << '\n';
			}
		}
		//command: ./uniq -d
		else if (dupsonly == 1) {
			if (i->second > 1)
				cout << i->first << '\n';
		}
		//command: ./uniq -u
		else if (uniqonly == 1) {
			if (i->second == 1)
				cout << i->first << '\n';
		}
	}

	return 0;
}

	/* TODO: write me... */

