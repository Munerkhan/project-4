#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */

set<string> multisetToSet(multiset<string> const i);

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	multiset<string> S1;	//no arguments
	set<string> S2; //unique arguement set
	multiset<string,igncaseComp> S3;
	set<string,igncaseComp> S4;


	string x;
	while(getline(cin,x))
	{
		S1.insert(x);
		S2.insert(x);
		S3.insert(x);
		S4.insert(x);
	}
	multiset<string>::iterator i;
	multiset<string>::reverse_iterator ri; //geekforgeeks reverse iterator - also asked professor marty


	if(descending == 1) {
		// command ./uniq -r -f -u
		if (ignorecase == 1 && unique == 1) {
			for(ri = S4.rbegin(); ri != S4.rend(); ri++)
			{
				cout << *ri <<'\n';
			}
			return 0;
		}
		//commandd ./uniq -r -u
		if(unique == 1) {
			for(ri = S2.rbegin(); ri!= S2.rend(); ri++)
			{
				cout<< *ri <<'\n';
			}
		}
		//command ./uniq -r -f
		else if (ignorecase== 1) {
			for(ri = S3.rbegin(); ri != S3.rend(); ri++)
			{
				cout << *ri << '\n';
			}
		}
		//command ./uniq -r
		else {
			for(ri = S1.rbegin(); ri!= S1.rend(); ri++)
			{
				cout << *ri << '\n';
			}
		}
	}
	else {
		//command ./uniq -f -u
		if (ignorecase == 1 && unique == 1) {
			for(i = S4.begin(); i != S4.end(); i++)
			{
				cout << *i <<'\n';
			}
			return 0;
		}
		//command ./uniq -u
		if(unique == 1) {
			for(i = S2.begin(); i!= S2.end(); i++)
			{
				cout<< *i <<'\n';
			}
		}
		//command ./uniq -f
		else if (ignorecase== 1) {
			for(i = S3.begin(); i != S3.end(); i++)
			{
				cout << *i << '\n';
			}
		}
		//command ./uniq   "else: solves the no argument"
		else {
			for(i = S1.begin(); i!= S1.end(); i++)
			{
				cout << *i << '\n';
			}
		}
	}
	return 0;
}
