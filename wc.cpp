/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */
#include <iostream>
using std::cin;
using std::cout;
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

		string input;
	int words = 0;
	int lines = -1;
	int chars = 0;
	int maxLen = 0;

	string max;


	while(getline(cin, input))
	{
		int len= input.length();
		int num=0;


		//incremeting the maxline length

		if (maxLen<len)
		{
			max=input;
			maxLen=len;
		}


		//number of lines increments here

		lines++;

		//number of characters
		chars+=(len);

		if (len<1)
		{
			words--;
		}

		char last= input[len-1];
		int last1 = isspace(last);
		if (last1==0)
		{
			words++;
		}

		for (int i=1;i<len;i++)
		{
			// this if statements checks if the previous char is character and next is white space
			if (isspace(input[i-1])==0&&isspace(input[i])!=0)
			{
				words++;
			}
		}




}



//maxLen code outside while loop
// found the line with the maximum length
int tabs=0;
int Lbytes=0;
int len2 = max.length();
for (int i =0;i<len2;i++)
{
	if (max[i]=='\t')
	{

		tabs++;

		Lbytes+=8-Lbytes%8;

		//i=i+(8-len2%8);
		//cout<<"tab at = "<<i<<"   i mod 8 = "<<i%8<<"\n";

		Lbytes--;
	}
	Lbytes++;
}

//cout <<"tabs= "<<tabs<<"\n";
//cout <<"bytes = "<<bytes<<"\n";











	if(argc==1)
	{
		printf(" %i %i %i\n",lines,words,chars+lines);
	}
	else if (charonly==1&&linesonly==1&&wordsonly==1&&longonly==1)
	{
		printf("%i %i %i %i \n",lines,words,chars+lines,Lbytes);
	}
	else if (linesonly==1&&wordsonly==1&&longonly==1)
	{
		printf("%i %i %i\n",lines,words,Lbytes);
	}
	else if (linesonly==1&&charonly==1&&wordsonly==1)
	{
		printf("%i %i %i \n",lines,words,chars+lines);
	}
	else if (linesonly==1&&longonly==1&&charonly==1)
	{
		printf("%i %i %i\n",lines,chars+lines,Lbytes);
	}
		else if (linesonly==1&&wordsonly==1&&charonly==1)
	{
		printf(" %i %i %i\n",lines,words,chars+lines);
	}
	else if (charonly==1&&wordsonly==1&&longonly==1)
	{
		printf("%i %i %i \n",words,chars+lines,Lbytes);
	}
	else if (charonly==1&&longonly==1)
	{
		printf("%i %i\n",chars+lines,Lbytes);
	}

	else if (wordsonly==1&&longonly==1)
	{
			printf("%i %i\n",words,Lbytes);
	}
	else if (linesonly==1&&longonly==1)
	{
		printf("%i %i\n",lines,Lbytes);
	}
	else if (charonly==1&&linesonly==1)
	{
		printf("%i %i\n",lines,chars+lines);
	}
	else if (charonly==1&&wordsonly==1)
	{
		printf(" %i %i\n",words,chars+lines);
	}
	else if (linesonly==1&&wordsonly==1)
	{
			printf(" %i %i\n",lines,words);
	}

	else
	{
		if (wordsonly==1)
		{
			cout <<words <<"\n";
		}
		if(linesonly==1)
		{
			cout <<lines <<"\n";
		}
		if(charonly==1)
		{
			cout<<chars+lines<<"\n";
		}
		if (longonly==1)
		{
			cout<<Lbytes<<"\n";
		}


}

return 0;

}



//unique words aftr this
/*vector <string> extractwords (string str)
{
	vector<string> L;
	int len = str.length();
	int begin = 0;
	int end = 0;

	string word = "";

	for (int i=0;i<len;i++)
	{
		for (int j=i;j<len;j++)
		{
			if((str[i]== ' ')&&str[j] !=' ')
			{
				begin = i;
			}

			if((str[j]== ' '||str[j]== '\t')&&str[i] !=' ')
			{
				end = j;
				i=j;
			}
			int wlen= end-begin;
			word = str.substr(begin,wlen);




		}
		L.push_back(word);
	}

	return L;

}*/
