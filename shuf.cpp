#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>

using namespace std;
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */

	size_t count1 = count;

		if (echo == 1){

			vector<string> v;
			while (optind < argc){
			v.push_back(argv[optind++]);
				}

				random_shuffle (v.begin() , v.end()); //v.begin()+1 (wont shuffle the first)

				for (size_t i =0 ; i<v.size(); i++){
					cout << v[i] << '\n';
					}
				}




		else if (count1 != -1)	{

				string input;
				vector<string> his;
				while(getline(cin,input))
				{
					his.push_back(input);
				}

				size_t p;

				random_shuffle(his.begin(),his.end());

				if (his.size()< count1){
					p = his.size();
					}
				else
					p = count1;


			for (size_t k = 0; k<p; k++)
				{
					cout << his[k] << '\n';
				}
			}




			else if (userange == true){
				srand(time(0));
				vector <int> minusi;
				int low = rlow;
				int high = rhigh;
				int sizeV = high - low +1;
				for (int k = low ; k <= high; k++){

					minusi.push_back (k);

					}

				for (int k =0 ; k<= sizeV -1 ; k++){
							int r = k+rand()%(sizeV-k);
					swap (minusi[k],minusi[r]);
					}

				for (int j= 0; j<sizeV; j++){
					cout << minusi[j] << '\n';
					}
				}


		else{

			string input;
				vector<string> his;
				while(getline(cin,input))
				{
					his.push_back(input);
				}

				random_shuffle(his.begin(),his.end());

					for (size_t k = 0; k< his.size(); k++)
				{
					cout << his[k] << '\n';
				}
			}





return 0;
}
