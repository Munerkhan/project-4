/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * Baishaki Debi - replied from my question in Piazza, suggested to use fread instead of getline
 * www.stackoverflow.com/questions/10129085/read-from-stdin-write-to-stdout-in-c
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <map>
using std::map;
#include <vector>
using namespace std;
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.
#include <cstdio>
#define BLOCK_SIZE 5000

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);






	char C[BLOCK_SIZE];
	char n[BLOCK_SIZE];
	size_t bytes;


	if (comp == 1){


		while(!feof(stdin))
	{
		bytes = fread(C,1,BLOCK_SIZE,stdin);
		fwrite(C,1,bytes,stdin);
		fflush(stdout);
	}
		char k;

		k=  s2[s2.length()-1];

  for (size_t i = 0; i < bytes; i++){
		 n[i] = C[i];
		}





	for (size_t i = 0; i < bytes; i++){
		for (size_t j = 0; j < s1.length(); j++){

			if (C[i] == s1[j]){
				C[i] = k;
			}
		}

	}
	for (size_t p = 0; p< bytes; p++){
		if (C[p] == k){
				C[p] =n[p];
			}

		else {
			C[p] = k;
			}



		cout << C[p];

			}





		/*vector <char> imp;
		vector <char> v1;
		vector <char> v2;

	for (size_t i = 0; i <s1.length(); i++){
		v1.push_back(s1.at(i));
		}
	for (size_t j = 0; j <s2.length(); j++){
		v2.push_back(s2.at(j));
		}

		char in;
    string str;
		while (getline(cin ,str)) {
			for (size_t i =0; i < str.length() ; i++){
				in = str.at(i);
			imp.push_back(in);
				}
			imp.push_back('\n');

			}
				char k;
				//k = s2.at(s2.length()-1);
			k = v2[v2.size()-1]; // assign the last value of the string 2

		for(size_t i = 0 ; i<imp.size();i++){
			for (size_t j =0; j< v1.size(); j++){
				int ch,ch1;
				char l,m;
				l = imp[i];
				m= v1[j];
				 ch =l;
				 ch1= m;
				//cout << ch << " " << ch1 ;
			 if (ch == ch1){
				 imp[i] = k;
			 }
			 else {
				 imp[i] = k;
			 }

		 }
		cout << imp[i];
	}

*/
	}









	else{
	//will read the std input as a file : www.stackoverflow.com
	//this will include whitespace as std input and will solve the extra new line at the end of output



	while(!feof(stdin))
	{
		bytes = fread(C,1,BLOCK_SIZE,stdin);
		fwrite(C,1,bytes,stdin);
		fflush(stdout);
	}
	//will compare every character to the
	for (size_t i = 0; i < bytes; i++){
		for (size_t j = 0; j < s1.length(); j++){
			if (C[i] == s1[j]){
				C[i] = s2[j];
			}
		}
		cout << C[i];
	}


}
	/* TODO: finish this... */

	return 0;
}
